const restify = require('restify');
const data = require('./data.json');
const server = restify.createServer();

server.get('/:offset/:length', (req, res, next) => {
  // const offset = parseInt(req.params.offset, 10);
  const length = parseInt(req.params.length, 10);
  res.send(data.slice(0, length), { 'Access-Control-Allow-Origin': '*' });
  next();
});

server.listen(4201, function() { console.log('%s listening at %s', server.name, server.url) });
