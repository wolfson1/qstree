import { Pipe as NgPipe, PipeTransform } from '@angular/core';

@NgPipe({
  name: 'numeric'
})
export class Pipe implements PipeTransform { 
  public transform(v: any): number {
    return Number(v);
  } 
}