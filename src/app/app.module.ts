import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { QSCTreeModule } from './tree';
import { QSCOperationsModule } from './operations';
import { Module as QSCNodeStateModule } from './operations/nodestate';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Pipe as NumericPipe } from './numeric.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NumericPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    QSCTreeModule,
    QSCOperationsModule,
    HttpClientModule,
    ScrollingModule,
    QSCNodeStateModule
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
