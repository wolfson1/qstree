export enum OperationType { DELETE, EDIT, REPARENT, ADDCHILD, DUPLICATE };
export type OperationDescriptor = OperationDelete | OperationEdit | OperationReparent | OperationAddChild | OperationDuplicate;
export type OperationCompiled = (v: OperationTarget, index: number, list: OperationTarget[]) => OperationTarget[];
export type OperationTarget = {
  id: number;
  parent?: number;
  value: string;
}
export type LeveledOperationTarget = (OperationTarget & {level: number});
export type GroupedOperation = {
  id: number;
  actions: OperationDescriptor[];
}

export type OperationDelete = {
  type: OperationType.DELETE;
  id: number;
}

export type OperationEdit = {
  type: OperationType.EDIT;
  id: number;
  value: string;
}

export type OperationReparent = {
  type: OperationType.REPARENT;
  id: number;
  parent: number;
}

export type OperationAddChild = {
  type: OperationType.ADDCHILD;
  id: number;
  value?: string;
}

export type OperationDuplicate = {
  type: OperationType.DUPLICATE;
  id: number;
}
