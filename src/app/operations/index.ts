export { Module as QSCOperationsModule } from './module';
export { OperationDescriptor, OperationType, OperationTarget } from './types';
export { apply } from './helpers';
