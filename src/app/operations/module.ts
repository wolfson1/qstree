import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { GroupPipe } from './group.pipe';
import { ApplyPipe } from './apply.pipe';
import { Pipe as IsDeletingPipe } from './isdeleting.pipe';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ Component, GroupPipe, ApplyPipe, IsDeletingPipe ],
  exports: [ Component, ApplyPipe ]
})
export class Module { }