import { Pipe, PipeTransform } from '@angular/core';
import { OperationTarget, OperationDescriptor } from './types';
import { Observable } from 'rxjs';
import { combineLatest, map } from 'rxjs/operators';
import { apply } from './helpers';

@Pipe({
  name: 'apply',
  pure: true
})
export class ApplyPipe implements PipeTransform {
  public transform(data: Observable<OperationTarget[]>, ops: Observable<OperationDescriptor[]>, sorted: (OperationTarget & {level: number})[]): Observable<OperationTarget[]> {
    return data
      .pipe(
        combineLatest(ops),
        map(([data, ops]) => apply(ops, data, sorted)));
  }
}