import { Pipe, PipeTransform } from '@angular/core';
import { OperationDescriptor, GroupedOperation } from './types';
import { toPairs, Dictionary } from 'ramda';

@Pipe({
  name: 'group',
  pure: true
})
export class GroupPipe implements PipeTransform {
  public transform(ops: OperationDescriptor[]): GroupedOperation[] {
    return toPairs(ops
      .reduce((acc, v) => ({
        ...acc,
        [v.id]: [ ...(acc[v.id] || []), v ]
      }), {} as Dictionary<OperationDescriptor[]>))
      .map(([id, actions]) => ({id: parseInt(id, 10), actions }))
  }
}