import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { OperationType } from './types';

@NgPipe({
  name: 'isdeleting'
})
export class Pipe implements PipeTransform {
  public transform(data: {type: OperationType}[]): boolean {
    return data.filter(v => v.type === OperationType.DELETE).length > 0;
  }
}