import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { Pipe as StatePipe } from './state.pipe';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ Component, StatePipe ],
  exports: [ Component, StatePipe ]
})
export class Module {}
