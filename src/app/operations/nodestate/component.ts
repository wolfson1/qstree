import { Component as NgComponent, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges, HostBinding } from '@angular/core';
import { OperationType } from '../types';

@NgComponent({
  selector: '[qsc][nodestate]',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component implements OnChanges {
  @Input() public nodestate: OperationType[] = [];
  @HostBinding('class.deleting') public deleting: boolean = false;

  public readonly variants = OperationType;
  public readonly tracker = (_, v: OperationType) => v;

  public ngOnChanges({nodestate}: SimpleChanges): void {
    this.deleting = (nodestate.currentValue as OperationType[]).indexOf(OperationType.DELETE) >= 0;
  }



}
