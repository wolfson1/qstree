import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { OperationDescriptor, OperationType } from '../types';

@NgPipe({
  name: 'state',
  pure: true
})
export class Pipe implements PipeTransform {
  public transform(ops: OperationDescriptor[], id: number): OperationType[] {
    return ops
      .filter(op => op.id === id)
      .map(op => op.type);
  }
}