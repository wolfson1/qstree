import { OperationDescriptor, OperationTarget, OperationType, OperationCompiled, LeveledOperationTarget, OperationReparent, OperationDuplicate, OperationEdit, OperationAddChild, OperationDelete } from './types';


export const apply = (ops: OperationDescriptor[], targets: OperationTarget[], sorted: LeveledOperationTarget[]) => {
  return targets
    .reduce((acc, target, index, list) =>
      [ ...acc,
        ...ops
          .filter(operation => operation.id === target.id)
          .map(operation => compile(operation, sorted))
          .reduce((_, operation) => operation(target, index, list), [ target ] as typeof targets) ],
      [] as typeof targets);
};

export const compile: ((op: OperationDescriptor, sorted: LeveledOperationTarget[]) => OperationCompiled) = (op, sorted) => {
  switch (op.type) {
    case OperationType.DELETE: return compileOperationDelete(op, sorted);
    case OperationType.ADDCHILD: return compileOperationAddChild(op);
    case OperationType.EDIT: return compileEditOperation(op);
    case OperationType.REPARENT: return compileReparentOperation(op);
    case OperationType.DUPLICATE: return compileDuplicateOperation(op, sorted);
  }
}




function maxid(list: {id?: number}[]): number {
  return list.reduce((acc, v) => v.id > acc ? v.id : acc, 0);
}

function compileOperationDelete(operation: OperationDelete, sorted: LeveledOperationTarget[]): OperationCompiled {
  const startIndex = sorted.findIndex(v => v.id === operation.id);
  const endIndex = sorted.findIndex((v, i, list) => i > startIndex && v.level <= list[startIndex].level);
  const nodesToRemove = sorted
    .slice(startIndex, endIndex)
    .map(v => v.id);
  return (target: OperationTarget) => {
    return nodesToRemove.includes(target.id) ? [] : [target];
  };
}

function compileOperationAddChild(operation: OperationAddChild): OperationCompiled {
  return (target: OperationTarget, _, list) => target.id === operation.id
    ? [ target,
        { parent: operation.id,
          value: '',
          id: 1 + maxid(list) }]
    : [target]
}

function compileEditOperation(operation: OperationEdit): OperationCompiled {
  return (target: OperationTarget) => target.id === operation.id ? [{...target, value: operation.value}] : [target];
}

function compileReparentOperation(operation: OperationReparent): OperationCompiled {
  return (target: OperationTarget) => target.id === operation.id ? [{...target, parent: operation.parent}] : [target];
}

function compileDuplicateOperation(operation: OperationDuplicate, sorted: LeveledOperationTarget[]): OperationCompiled {
  const startIndex = sorted.findIndex(v => v.id === operation.id);
  const endIndex = sorted.findIndex((v, i, list) => i > startIndex && v.level <= list[startIndex].level);
  const maxId = maxid(sorted);
  const wayBack = [{ level: sorted[startIndex].level - 1, id: sorted[startIndex].parent }];
  const duplicated = sorted
    .slice(startIndex, endIndex)
    .reduce((acc, item, index, list) => {
      const wayBackLastStep = wayBack[wayBack.length - 1];

      const parent = wayBackLastStep.id;
      const value = item.value;
      const id = maxId + index + 1;
      const level = item.level;

      const newitem = { id, parent, value, level };

      if (list[index + 1])
        if (list[index + 1].level > item.level) wayBack.push(newitem);
        else if (list[index + 1].level < item.level) wayBack.splice(-1, item.level - list[index + 1].level);

      return [ ...acc, newitem ]
    },
    [] as typeof sorted);
  return (target: OperationTarget) => target.id === operation.id
    ? [ target, ...duplicated]
    : [ target ];
}