import { Component as NgComponent, ChangeDetectionStrategy, Input, Output, EventEmitter, ContentChild, TemplateRef } from '@angular/core';
import { OperationDescriptor, OperationType, GroupedOperation } from './types';
import { trigger, transition, style, animate } from '@angular/animations';

@NgComponent({
  selector: '[qsc][operations]',
  styleUrls: ['component.scss'],
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('animate', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('.2s ease-in-out', style({ height: '*', opacity: 1 }))]),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        animate('.2s ease-in-out', style({ height: 0, opacity: 0 }))])])]
})
export class Component {
  @Input() public operations: OperationDescriptor[] = [];
  @Output() public readonly operations$ = new EventEmitter<OperationDescriptor[]>();

  @ContentChild(TemplateRef, { static: true }) public readonly template: TemplateRef<GroupedOperation> | null = null;

  public cancelgroup(group: GroupedOperation): void { this.operations$.emit(this.operations.filter(v => !group.actions.includes(v))) }
  public cancelop(op: OperationDescriptor): void { this.operations$.emit(this.operations.filter(v => v !== op)) }
  public update(op: OperationDescriptor, value: string): void { this.operations$.emit(this.operations.map(v => v === op ? { ...v, value } : v)) }

  public readonly tracker = (_, v: {id: string}) => v.id;
  public readonly typetracker = (_, v: {type: OperationType}) => v.type;
  public readonly variants = OperationType;
}
