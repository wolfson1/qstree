import { Component as NgComponent, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { trigger, transition, style, animate } from '@angular/animations';
import { BehaviorSubject, Subject } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import { TreeNode, FlatTreeItem } from 'src/app/tree';
import { OperationDescriptor, OperationType, apply } from 'src/app/operations';
import * as data from './data.json';

@NgComponent({
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  animations: [
    trigger('animate', [
      transition(':enter', [
        style({ opacity: 0, height: 0 }),
        animate('.2s ease-in-out', style({ opacity: 1, height: '*' }))]),
      transition(':leave', [
        style({ opacity: 1, height: '*' }),
        animate('.2s ease-in-out', style({ opacity: 0, height: 0 }))])])]
})
export class Component implements OnDestroy {
  public readonly database$ = new BehaviorSubject<TreeNode[]>(data);
  public readonly cache$ = new BehaviorSubject<TreeNode[]>([]);
  public readonly actions$ = new BehaviorSubject<OperationDescriptor[]>([]);
  
  public load(node: TreeNode): void { this.actionLoad$.next(node) }
  
  public delete(id: number): void { this.actionRegister$.next({ type: OperationType.DELETE, id }) }
  public edit(id: number, value: string): void { this.actionRegister$.next({ type: OperationType.EDIT, id, value }) }
  public add(id: number, value: string): void { this.actionRegister$.next({ type: OperationType.ADDCHILD, id, value }) }
  public duplicate(id): void { this.actionRegister$.next({ type: OperationType.DUPLICATE, id }) }

  public apply(ops: OperationDescriptor[], sorted: FlatTreeItem[]): void { this.actionExecute$.next({ops, sorted}) }
  public reset(): void {
    this.database$.next(data);
    this.cache$.next([]);
    this.actions$.next([]);
  }

  public ngOnDestroy(): void { this.subscription.unsubscribe() } 

  
  

  private readonly actionLoad$ = new Subject<TreeNode>();
  private readonly actionRegister$ = new Subject<OperationDescriptor>();
  private readonly actionExecute$ = new Subject<{ops: OperationDescriptor[]; sorted: FlatTreeItem[]}>();
  private readonly actionReset$ = new Subject<void>();

  private readonly subscription = this.actionLoad$
    .pipe(
      withLatestFrom(this.cache$),
      map(([fresh, current]) => [ fresh, ...current
        .filter(v => v.id !== fresh.id) ]))
    .subscribe(v => this.cache$.next(v))
  
    .add(this.actionRegister$
      .pipe(
        withLatestFrom(this.actions$),
        map(([fresh, current]) =>
          [ fresh,
            ...current
              .filter(v => !(v.id === fresh.id && v.type === fresh.type)) ]))
      .subscribe(ops => this.actions$.next(ops)))
    
    .add(this.actionExecute$
      .pipe(
        withLatestFrom(this.database$, this.cache$))
      .subscribe(([{ops, sorted}, database, cache]) => {
        // this.database$.next(apply(database, ops));
        this.cache$.next(apply(ops, cache, sorted));
        this.actions$.next([]);
      }))
}
