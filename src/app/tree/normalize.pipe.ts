import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { TreeNode, FlatTreeItem } from './types';
import { normalize } from './helpers';

@NgPipe({
  name: 'normalizeTree',
  pure: true
})
export class Pipe implements PipeTransform {
  public transform(data: TreeNode[], depth = 5): FlatTreeItem[] {
    return normalize(data, depth);
  }
}
