import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgModule } from '@angular/core';
import { Component } from './component';
import { Directive as NodeDirective } from './node.directive';
import { CommonModule } from '@angular/common';
import { Pipe as NormalizePipe } from './normalize.pipe';

@NgModule({
  imports: [CommonModule, ScrollingModule],
  declarations: [ Component, NodeDirective, NormalizePipe ],
  exports: [ Component, NodeDirective, NormalizePipe ]
})
export class Module { }
