import { FlatTreeItem, TreeNode } from './types';
import { reduce } from 'ramda';
import * as arrayToTree from 'array-to-tree';

export function normalize(data: TreeNode[], depth = 5): FlatTreeItem[] {
  return flatten(arrayToTree(data, { parentProperty: 'parent', childrenProperty: 'children' }), depth);
}

function flatten(nodes: arrayToTree.Tree<TreeNode>[], maxdepth = Number.POSITIVE_INFINITY): FlatTreeItem[] {
  return flattenTrees(nodes, 0, maxdepth);
}

function flattenTrees(nodes: arrayToTree.Tree<TreeNode>[], level: number, maxdepth: number, parent: number | null = null, prefix: FlatTreeItem['prefix'] = []): FlatTreeItem[] {
  return reduce((acc, v) => {
    if (level <= maxdepth) {
      const lastchild = nodes[nodes.length - 1] === v;
      const leaf = level === maxdepth || !v.children || v.children.length === 0;
      const hidden = level === maxdepth && v.children && v.children.length || 0;
      const { value, id } = v;
      const nextprefix: typeof prefix = level > 0 ? [...prefix, lastchild ? ' ' : '|'] : [];
      return [
        ...acc,
        { id, parent, value, prefix, level, leaf, lastchild, hidden },
        ...flattenTrees(v.children || [], level + 1, maxdepth, id, nextprefix )
      ];
    }
    return acc;
  },
  [] as FlatTreeItem[],
  nodes)
}
