import { Directive as NgDirective, TemplateRef } from '@angular/core';
import { TreeNode } from './types';

@NgDirective({
  selector: '[qst-tree-node]',
  exportAs: 'qst-tree-node'
})
export class Directive {
  constructor( public readonly template: TemplateRef<TreeNode> ) {}
}