import { Component as NgComponent, ChangeDetectionStrategy, Input, ContentChild, Output, EventEmitter, OnDestroy, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { Directive as TreeNodeDirective } from './node.directive';
import { TreeNode, FlatTreeItem } from './types';
import { Subject, ReplaySubject } from 'rxjs';
import { withLatestFrom, filter, map } from 'rxjs/operators';
import { trigger, transition, style, animate } from '@angular/animations';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

@NgComponent({
  selector: '[qsc][tree]',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('animate', [
      transition('false => true', [
        style({ backgroundColor: 'rgba(0, 0, 255, .5)' }),
        animate('2s ease-in-out', style({ backgroundColor: 'rgba(0, 0, 255, 0)' }))])])]
})
export class Component implements OnDestroy, OnChanges {
  @Input() public reparent = true;
  @Input() public tree: FlatTreeItem[] = [];
  @Input() public scrollto: number = Number.NaN;
  @ContentChild(TreeNodeDirective, { static: true }) public readonly renderer: TreeNodeDirective;

  @Output() public readonly reparent$ = new EventEmitter<[TreeNode, TreeNode]>();
  @Output() public readonly scrollto$ = new EventEmitter<number>();

  public readonly tracker = (_, v: {id: number}) => v.id;
  public readonly dropped$ = new Subject<FlatTreeItem>();
  public readonly dragging$ = new ReplaySubject<FlatTreeItem | null>(1);

  public dragtarget: HTMLLIElement | null = null;

  public ngOnDestroy(): void { this.subscription.unsubscribe(); }
  public ngOnChanges({scrollto, tree, depth}: SimpleChanges): void {
    if (scrollto && !Number.isNaN(scrollto.currentValue)) {
      this.vsviewport.scrollToIndex(this.tree.findIndex(v => v.id === scrollto.currentValue));
    }
  }



  @ViewChild(CdkVirtualScrollViewport, { static: true }) private readonly vsviewport: CdkVirtualScrollViewport;
  
  private readonly subscription = this.dropped$
    .pipe(
      map(v => ({ id: v.id, parent: v.parent, value: v.value })),
      withLatestFrom(this.dragging$
        .pipe(
          filter(v => !!v),
          map(v => ({ id: v.id, parent: v.parent, value: v.value })))))
    .subscribe(v => this.reparent$.emit(v));
}