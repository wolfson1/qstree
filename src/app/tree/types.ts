export type TreeNode = {
  parent?: number;
  id: number;
  value: string;
}

export type FlatTreeItem = {
  id: number;
  value: string;
  level: number;
  lastchild: boolean;
  leaf: boolean;
  hidden?: number;
  parent: number;
  prefix: ('|' | ' ')[];
}
