import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [RouterModule.forRoot([{
    path: 'bigdata',
    pathMatch: 'prefix',
    loadChildren: () => import('./+bigdata/module').then(v => v.Module)
  }, {
    path: 'test',
    pathMatch: 'prefix',
    loadChildren: () => import('./+test/module').then(v => v.Module)
  }, {
    path: '',
    pathMatch: 'prefix',
    redirectTo: 'test'
  }])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
