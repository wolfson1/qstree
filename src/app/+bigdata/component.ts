import { Component as NgComponent, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { trigger, transition, style, animate } from '@angular/animations';
import { BehaviorSubject, Subject } from 'rxjs';
import { concatMap, map, withLatestFrom, share } from 'rxjs/operators';
import { TreeNode, FlatTreeItem } from 'src/app/tree';
import { OperationDescriptor, OperationType, apply } from 'src/app/operations';
import { environment } from 'src/environments/environment';

@NgComponent({
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  animations: [
    trigger('animate', [
      transition(':enter', [
        style({ width: 0, opacity: 0 }),
        animate('.2s ease-in-out', style({ width: '*', opacity: 1 }))]),
      transition(':leave', [
        style({ width: '*', opacity: 1 }),
        animate('.2s ease-in-out', style({ width: 0, opacity: 0 }))])])]
})
export class Component implements OnDestroy {z
  public readonly defaultCount = 10;
  public readonly maxDepth = 5;

  public readonly data$ = new BehaviorSubject<TreeNode[]>([]);
  public readonly pending$ = new BehaviorSubject<OperationDescriptor[]>([]);
  
  public more(count: number): void { this.more$.next(count) }
  
  public delete(id: number): void { this.register$.next({ type: OperationType.DELETE, id }) }
  public edit(id: number, value: string): void { this.register$.next({ type: OperationType.EDIT, id, value }) }
  public reparent(id: number, parent: number): void { this.register$.next({ type: OperationType.REPARENT, id, parent }) }
  public add(id: number): void { this.register$.next({ type: OperationType.ADDCHILD, id }) }
  public duplicate(id): void { this.register$.next({ type: OperationType.DUPLICATE, id }) }

  public apply(ops: OperationDescriptor[], trees: FlatTreeItem[]): void { this.execute$.next({ops, trees}) }

  public ngOnDestroy(): void { this.subscription.unsubscribe() } 
  
  constructor(public readonly network: HttpClient) {  }
  
  

  private readonly more$ = new BehaviorSubject<number>(this.defaultCount);
  private readonly register$ = new Subject<OperationDescriptor>();
  private readonly execute$ = new Subject<{ops: OperationDescriptor[], trees: FlatTreeItem[]}>();

  private readonly subscription = this.more$
    .pipe(
      withLatestFrom(this.data$
        .pipe(
          map(items => items.reduce((acc, v) => v.id > acc ? v.id : acc, 0)))),
      concatMap(([count, max]) => this.network
        .get<TreeNode[]>(environment
          .get(max, count))
        .pipe(
          map(fresh => [fresh, max] as [typeof fresh, typeof max]))),
      withLatestFrom(this.data$),
      map(([[fresh, max], current]) =>
        [ ...current,
          ...fresh
            .map((node, index) => ({...node, id: max + index + 1}))]),
      share())
    .subscribe(v => this.data$.next(v))
  
    .add(this.register$
      .pipe(
        withLatestFrom(this.pending$),
        map(([fresh, current]) =>
          [ fresh,
            ...current
              .filter(v => !(v.id === fresh.id && v.type === fresh.type)) ]))
      .subscribe(ops => this.pending$.next(ops)))
    
    .add(this.execute$
      .pipe(
        withLatestFrom(this.data$),
        map(([{ops, trees}, data]) => apply(ops, data, trees)))
      .subscribe(data => {
        this.data$.next(data);
        this.pending$.next([]);
      }))
}
