import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, } from '@angular/core';
import { Component } from './component';
import { HttpClientModule } from '@angular/common/http';
import { QSCTreeModule } from 'src/app/tree';
import { QSCOperationsModule } from 'src/app/operations';
import { Module as QSCNodeStateModule } from 'src/app//operations/nodestate';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { Pipe as NumericPipe } from './numeric.pipe';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    Component,
    NumericPipe
  ],
  imports: [
    CommonModule,
    QSCTreeModule,
    QSCOperationsModule,
    HttpClientModule,
    ScrollingModule,
    QSCNodeStateModule,
    
    RouterModule.forChild([{
      path: '',
      component: Component
    }])
  ],
  bootstrap: [Component]
})
export class Module { }
