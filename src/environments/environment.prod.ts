export const environment = {
  production: true,
  get: (offset: number, length: number) => `https://my.api.mockaroo.com/plain_tree/${offset}/${length}?key=99fd30f0`
};
